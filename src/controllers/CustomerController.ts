import { validate } from "class-validator"
import { Request, Response } from "express"
import { getRepository } from "typeorm"
import { Basket } from "../entities/basket.entity"
import { Customer } from "../entities/customer.entity"

class CustomerController {  

  static getOne = async (req: Request, res: Response) => {
    const id: string = req.params.id;
    let customerRepository = getRepository(Customer);
    let customer: Customer;
    try {
      customer = await customerRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Customer not found");
      return;
    }
    res.status(200).send(customer);
  }

  static getAll = async (req: Request, res: Response) => {
    const customerRepository = getRepository(Customer);
    const customers = await customerRepository.find();

    res.send(customers);
  };


  static getBasketUser = async (req: Request, res: Response) => {
    const id: string = req.params.id;
    let basketRepository = getRepository(Basket);
    let basket: Basket;
    try {
      basket = await basketRepository.findOneOrFail({where:[ {customerId : id}]});
    } catch (error) {
      res.status(404).send("Basket not found");
      return;
    }
    res.status(200).send(basket);
  }

  static createOne = async (req: Request, res: Response) => {
    const customer = new Customer();
    customer.name = req.body.name;
    customer.email = req.body.email;
    customer.address = req.body.address;
    customer.phoneNumber = req.body.phoneNumber;
    
    const customerRepository = getRepository(Customer);    
      await customerRepository.save(customer)
      .then((saved) => {
        customer.id = saved.id;
      }).catch((ex) => {
        res.status(500).send("internal error");
      });
   
    res.status(200).send(customer);
  };

  static updateOne = async (req: Request, res: Response) => {
    const id = req.params.id;
    const partialCustomer = { ...req.body };

    const customerRepository = getRepository(Customer);

    try {
      await customerRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Customer not found");
      return;
    }

    const errors = await validate(partialCustomer);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    try {
      await customerRepository.update(id, partialCustomer);
    } catch (e) {
      res.status(500).send("Internal error");
      return;
    }
    res.status(201).send();
  };

  static deleteOne = async (req: Request, res: Response) => {
    const id = req.params.id;

    const customerRepository = getRepository(Customer);

    try {
      await customerRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Customer not found");
      return;
    }
    customerRepository.delete(id);

    res.status(204).send();
  };
}
export default CustomerController
