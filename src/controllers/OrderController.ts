import { validate } from "class-validator"
import { Request, Response } from "express"
import { getRepository } from "typeorm"
import { Order } from "../entities/order.entity"

class OrderController {  

  static getOne = async (req: Request, res: Response) => {
    const id: string = req.params.id;
    let orderRepository = getRepository(Order);
    let order: Order;
    try {
      order = await orderRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Order not found");
      return;
    }
    res.status(200).send(order);
  }

  static getAll = async (req: Request, res: Response) => {
    const orderRepository = getRepository(Order);
    const orders = await orderRepository.find();

    res.send(orders);
  };



  static createOne = async (req: Request, res: Response) => {
    const order = new Order();
    order.customerId = req.body.customerId;
    order.productId = req.body.productId;
    order.orderDate = new Date();
    order.price = req.body.price;
    const orderRepository = getRepository(Order);

    try {
      await orderRepository.save(order);
    } catch (e) {
      res.status(500).send("internal error");
    }

    res.status(200).send(order);
  };

  static updateOne = async (req: Request, res: Response) => {
    const id = req.params.id;
    const partialOrder = { ...req.body };

    const orderRepository = getRepository(Order);

    try {
      await orderRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Order not found");
      return;
    }

    const errors = await validate(partialOrder);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    try {
      await orderRepository.update(id, partialOrder);
    } catch (e) {
      res.status(500).send("Internal error");
      return;
    }
    res.status(201).send();
  };

  static deleteOne = async (req: Request, res: Response) => {
    const id = req.params.id;

    const orderRepository = getRepository(Order);

    try {
      await orderRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Order not found");
      return;
    }
    orderRepository.delete(id);

    res.status(204).send();
  };
}
export default OrderController
