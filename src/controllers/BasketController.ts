import { validate } from "class-validator"
import { create } from "domain"
import { Request, Response } from "express"
import { getRepository, LockNotSupportedOnGivenDriverError } from "typeorm"
import { Basket } from "../entities/basket.entity"
import { Customer } from "../entities/customer.entity"
import { Product } from "../models/product.model"

class BasketController {  

  static getOne = async (req: Request, res: Response) => {
    const id: string = req.params.id;
    let basketRepository = getRepository(Basket);
    let basket: Basket;
    try {
      basket = await basketRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Basket not found");
      return;
    }
    res.status(200).send(basket);
  }

  static getAll = async (req: Request, res: Response) => {
    const basketRepository = getRepository(Basket);
    const baskets = await basketRepository.find();

    res.send(baskets);
  };

  createOne = async (productsId : string, customerId:string) => {
    const basket = new Basket();
    basket.productsId = productsId//req.body.productsId;
    basket.customerId = customerId//req.body.customerId;
    
    const basketRepository = getRepository(Basket);

    try {
      await basketRepository.save(basket);
    } catch (e) {
      //res.status(500).send("internal error");
    }

    //res.status(200).send(basket);
  };

  static updateOne = async (req: Request, res: Response) => {
    const id = req.params.id;
    const partialBasket = new Basket()
    partialBasket.productsId = req.body.productsId;
    partialBasket.customerId = req.body.customerId;
    let goodBasket
    const basketRepository = getRepository(Basket);

    try {
       goodBasket = await basketRepository.findOneOrFail({where : {customerId:req.body.customerId}});
    } catch (error) {
    
    
    const basketRepository = getRepository(Basket);

     goodBasket = await basketRepository.save(partialBasket);
//      createOne(req.body.productsId, req.body.customerId);
      res.status(404).send("Basket create");
      return;
    }

    const errors = await validate(partialBasket);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    try {
      await basketRepository.update(goodBasket.id, partialBasket);
    } catch (e) {
      console.log(e)
      res.status(500).send("Internal error");
      return;
    }
    res.status(201).send();
  };

  static deleteOne = async (req: Request, res: Response) => {
    const id = req.params.id;

    const basketRepository = getRepository(Basket);

    try {
      await basketRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Basket not found");
      return;
    }
    basketRepository.delete(id);

    res.status(204).send();
  };
}
export default BasketController
