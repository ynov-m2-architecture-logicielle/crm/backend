import { validate } from "class-validator"
import { Request, Response } from "express"
import { getRepository } from "typeorm"
import { Discount } from "../entities/discount.entity"

class DiscountController {  

  static getOne = async (req: Request, res: Response) => {
    const id: string = req.params.id;
    let discountRepository = getRepository(Discount);
    let discount: Discount;
    try {
      discount = await discountRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Discount not found");
      return;
    }
    res.status(200).send(discount);
  }

  static getAll = async (req: Request, res: Response) => {
    const discountRepository = getRepository(Discount);
    const discounts = await discountRepository.find();

    res.send(discounts);
  };



  static createOne = async (req: Request, res: Response) => {
    const discount = new Discount();
    discount.customerId = req.body.customerId;
    discount.productId = req.body.productId;
    discount.percentage = req.body.percentage;
                  
    const discountRepository = getRepository(Discount);

    try {
      await discountRepository.save(discount);
    } catch (e) {
      res.status(500).send("internal error");
    }

    res.status(200).send(discount);
  };

  static updateOne = async (req: Request, res: Response) => {
    const id = req.params.id;
    const partialDiscount = { ...req.body };

    const discountRepository = getRepository(Discount);

    try {
      await discountRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Discount not found");
      return;
    }

    const errors = await validate(partialDiscount);
    if (errors.length > 0) {
      res.status(400).send(errors);
      return;
    }

    try {
      await discountRepository.update(id, partialDiscount);
    } catch (e) {
      res.status(500).send("Internal error");
      return;
    }
    res.status(201).send();
  };

  static deleteOne = async (req: Request, res: Response) => {
    const id = req.params.id;

    const discountRepository = getRepository(Discount);

    try {
      await discountRepository.findOneOrFail(id);
    } catch (error) {
      res.status(404).send("Discount not found");
      return;
    }
    discountRepository.delete(id);

    res.status(204).send();
  };
}
export default DiscountController
