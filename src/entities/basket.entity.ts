import { IsEmail, IsNotEmpty } from "class-validator"
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, OneToOne, PrimaryGeneratedColumn, Unique } from "typeorm"
import { Product } from "../models/product.model";
import { Customer } from "./customer.entity";

@Entity()
export class Basket {

    @PrimaryGeneratedColumn("uuid")
    id: string

    @OneToOne(() => Customer)
    customer: Customer;
    @JoinColumn({ name: "customerId" })
    @Column({ name: "customerId", type: "uuid", nullable: true })
    customerId: string;
    
    @Column()
    productsId:string;

    //products : Product[]//TODO Request get to conf offer
    
}
