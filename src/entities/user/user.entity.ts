import { IsEmail, IsNotEmpty } from "class-validator"
import { Column, Entity, PrimaryGeneratedColumn, Unique } from "typeorm"

@Entity()
@Unique(["email"])
export class User {

    @PrimaryGeneratedColumn("uuid")
    id: string

    @Column()
    @IsNotEmpty()
    @IsEmail()
    email: string

    @Column()
    @IsNotEmpty()
    username: string

    constructor(user: Partial<User> = {}) {
        this.id = user.id
        this.username = user.username
    }
}
