import { IsEmail, IsNotEmpty } from "class-validator"
import { NOTFOUND } from "dns"
import { Column, Entity, PrimaryGeneratedColumn, Unique } from "typeorm"

@Entity()
export class Customer {

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    @IsNotEmpty()
    name:string;

    @Column()
    @IsNotEmpty()
    @IsEmail()
    email:string;

    @Column()
    @IsNotEmpty()
    address:string;

    @Column()
    @IsNotEmpty()
    phoneNumber:string;

    // constructor(user: Partial<Customer> = {}) {
       
    // }
}
