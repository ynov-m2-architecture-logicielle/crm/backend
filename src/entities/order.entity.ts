import { IsEmail, IsNotEmpty } from "class-validator"
import { Column, Double, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, Unique } from "typeorm"
import { PrimaryGeneratedColumnNumericOptions } from "typeorm/decorator/options/PrimaryGeneratedColumnNumericOptions";
import { Product } from "../models/product.model";
import { Customer } from "./customer.entity";

@Entity()
export class Order {

    @PrimaryGeneratedColumn("uuid")
    id: string    

    @OneToOne(() => Customer)
    customer: Customer;
    @JoinColumn({ name: "customerId" })
    @Column({ name: "customerId", type: "uuid", nullable: false })
    customerId: string;

    @Column()
    @IsNotEmpty()
    productId: string;

    product:Product; //TODO Request to conf offer

    @Column()
    @IsNotEmpty()
    orderDate : Date;

    @Column()
    @IsNotEmpty()
    price:string;
}
