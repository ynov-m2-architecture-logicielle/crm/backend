import { IsEmail, IsNotEmpty } from "class-validator"
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, Unique } from "typeorm"
import { Product } from "../models/product.model";
import { Customer } from "./customer.entity";

@Entity()
export class Discount {

    @PrimaryGeneratedColumn("uuid")
    id: string

    @OneToOne(() => Customer)
    customer: Customer;
    @JoinColumn({ name: "customerId" })
    @Column({ name: "customerId", type: "uuid", nullable: true })
    customerId: string;

    @Column()
    @IsNotEmpty()
    productId: string;

    product:Product; //TODO Request to conf offer

    @Column()
    @IsNotEmpty()
    percentage : number;
    
}
