import { Router } from "express"
import CustomerController from "../controllers/CustomerController"

const router = Router()

router.post("/createOne", CustomerController.createOne)
router.post("/updateOne/:id", CustomerController.updateOne)
router.delete("/delete/:id", CustomerController.deleteOne)
router.get("/:id/basket", CustomerController.getBasketUser)
router.get("/:id", CustomerController.getOne)
router.get("/", CustomerController.getAll)

export default router
