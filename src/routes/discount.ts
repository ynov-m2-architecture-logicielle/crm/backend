import { Router } from "express"
import DiscountController from "../controllers/DiscountController"

const router = Router()

router.post("/createOne", DiscountController.createOne)
router.post("/updateOne/:id", DiscountController.updateOne)
router.delete("/delete/:id", DiscountController.deleteOne)
router.get("/:id", DiscountController.getOne)
router.get("/", DiscountController.getAll)

export default router
