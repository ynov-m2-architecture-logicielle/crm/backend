import { Router } from "express"
import auth from "./auth"
import customer from "./customer"
import order from "./order"
import discount from "./discount"
import basket from "./basket"
import product from "./product"

const routes = Router()

routes.use("/auth", auth)
routes.use("/customer", customer)
routes.use("/order", order)
routes.use("/discount", discount)
routes.use("/basket", basket)

//Configurateur d'offre
routes.use("/api/product-config/product", product)

export default routes
