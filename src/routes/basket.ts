import { Router } from "express"
import BasketController from "../controllers/BasketController"

const router = Router()

//router.post("/createOne", BasketController.createOne)
router.post("/updateOne/:id", BasketController.updateOne)
router.delete("/delete/:id", BasketController.deleteOne)
router.get("/:id", BasketController.getOne)
router.get("/", BasketController.getAll)

export default router
