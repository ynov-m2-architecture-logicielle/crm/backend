import { Router } from "express"
import OrderController from "../controllers/OrderController"

const router = Router()

router.post("/createOne", OrderController.createOne)
router.post("/updateOne/:id", OrderController.updateOne)
router.delete("/delete/:id", OrderController.deleteOne)
router.get("/:id", OrderController.getOne)
router.get("/", OrderController.getAll)

export default router
