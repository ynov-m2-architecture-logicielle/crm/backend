import { Double } from "typeorm";
import { Image } from "./image.model";

export class Product {

    public id: string;
    public name:string;
    public reference : string;
    public price : Double;
    public description : string;
    public images : Image[];
    
    constructor() {}

}
