export class Tag {
    public id:string;
    public name:string;
    public category: Category
}

export enum Category {
    COLOR,
    TYPE,
    SEASON,
}