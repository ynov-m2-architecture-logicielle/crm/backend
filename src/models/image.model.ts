import { Double } from "typeorm";
import { Product } from "./product.model";

export class Image {

    public id:string;
    public extension : string;
    public product : Product;

}